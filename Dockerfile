FROM mcr.microsoft.com/dotnet/core/aspnet:3.0.1-alpine3.9

WORKDIR /user/src/dplusapp

COPY ./bin/Release/netcoreapp3.0/publish ./

EXPOSE 80

#ENTRYPOINT เหมือน CMD
ENTRYPOINT ["dotnet", "dplus-netcore.dll"]  
